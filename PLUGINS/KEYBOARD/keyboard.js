var KEYBOARD = {
  state: 0,
  searchText: "",
  shiftPressed: 0,
  capsLockPressed: 0
}


var loadCss = (url) => {
  var head  = document.getElementsByTagName('head')[0];
  var link  = document.createElement('link');
  link.rel  = 'stylesheet';
  link.type = 'text/css';
  link.href = url;
  link.media = 'all';
  head.appendChild(link);
}

loadCss("PLUGINS/KEYBOARD/keyboard.css");


var keyboardToggle = () => {
  var keyboardElement = document.getElementById('keyboard');
  if (KEYBOARD.state == 0) {
    keyboardElement.style.display = "block";
    KEYBOARD.state = 1;
  } else if (KEYBOARD.state == 1) {
    keyboardElement.style.display = "none";
    KEYBOARD.state = 0;
  }
}

var renderInput = () => {
  document.getElementById('keyboard-input').value = KEYBOARD.searchText;
  document.getElementById('keyboard-input').focus();
}

var keyboardUpdate = (x) => {
  KEYBOARD.searchText = x.value;
  console.log(KEYBOARD);
}
var keyboardSearch = () => {
  console.log(KEYBOARD.searchText);
}

var renderKey = () => {
  var elems = document.getElementsByTagName("key");
  for (var i = 0; i < elems.length; i++) {
    var specialClass = (x) => {
      var a = (KEYBOARD.capsLockPressed == 1) ? ((KEYBOARD.shiftPressed == 0) ? x.attributes.shift.value : x.attributes.unshift.value) : ((KEYBOARD.shiftPressed == 0) ? x.attributes.unshift.value : x.attributes.shift.value);

      if (a == "ctrl" || a == "alt" || a == "tab") return "keyboard-text-small";
      else if (a == "shift") return (KEYBOARD.shiftPressed == 1) ? "keyboard-text-small keyboard-button-active keyboard-text-left" : "keyboard-text-small keyboard-text-left";
      else if (a == "bshift")return (KEYBOARD.shiftPressed == 1) ? "keyboard-text-small keyboard-button-active keyboard-text-right" : "keyboard-text-small keyboard-text-right";
      else if (a == "caps")return (KEYBOARD.capsLockPressed == 1) ? "keyboard-text-small keyboard-button-active" : "keyboard-text-small";
      else if(a == "up" || a == "dn") return "keyboard-text-smaller keyboard-half-button";
      else if(a == "larrow" || a == "rarrow")return "keyboard-text-smaller keyboard-text-fix";
      return "";
    }
    var currentKey = (x) => {
      var a = (KEYBOARD.capsLockPressed == 1) ? ((KEYBOARD.shiftPressed == 0) ? x.attributes.shift.value : x.attributes.unshift.value) : ((KEYBOARD.shiftPressed == 0) ? x.attributes.unshift.value : x.attributes.shift.value);
      if (a == "space") return "";
      else if (a == "larrow") return "◀";
      else if (a == "rarrow") return "▶";
      else if (a == "up") return "▲";
      else if (a == "dn") return "▼";
      else if (a == "bs") return "←";
      else if (a == "enter") return "⤶";
      else if (a == "shift") return "↑";
      else if (a == "bshift") return "↑";
      else if (a == "win") return "⊞";
      else if (a == "backslash") return `\u{005C}`;
      return a;
    }
    var functionName = (x) => {
      var a = (KEYBOARD.capsLockPressed == 1) ? ((KEYBOARD.shiftPressed == 0) ? x.attributes.shift.value : x.attributes.unshift.value) : ((KEYBOARD.shiftPressed == 0) ? x.attributes.unshift.value : x.attributes.shift.value);
      if(a == "space") return `writeChar(' ')`;
      else if(a == "enter") return `keyboardSearch()`;
      else if(a == "bs") return `removeLastChar()`;
      else if(a == "caps") return `toggleCapsLock()`;
      else if(a == "backslash") return `writeBs()`;
      else if(a == "shift" || a == "bshift") return `shiftKey()`;
      else if(a == "tab") return `writeChar('\t')`;
      else if(a == "alt" || a == "ctrl" || a == "win") return `writeChar('')`;
      else if(a == "larrow") return `movePointer('left')`;
      else if(a == "rarrow") return `movePointer('right')`;
      else if(a == "up") return `movePointer('up')`;
      else if(a == "dn") return `movePointer('down')`;
      else return `writeChar('${a}')`;
    }
    elems[i].innerHTML = `
      <button class="keyboard-button-class ${specialClass(elems[i])}" style="width:${elems[i].attributes.size.value}px" onclick="${functionName(elems[i])}">
        ${currentKey(elems[i])}
      </button>
    `;
  }
}

var toggleCapsLock = () => {
  if (KEYBOARD.capsLockPressed == 1) KEYBOARD.capsLockPressed = 0;
  else KEYBOARD.capsLockPressed = 1;
  renderKey();
}
var writeChar = (x) => {
  KEYBOARD.searchText += x;
  if (KEYBOARD.shiftPressed == 1) shiftKey();
  renderInput();
}
var removeLastChar = () => {
  if(KEYBOARD.searchText != ""){
    KEYBOARD.searchText = KEYBOARD.searchText.slice(0,KEYBOARD.searchText.length - 1);
    renderInput();
  }
}

var shiftKey = () => {
  if (KEYBOARD.shiftPressed == 1) KEYBOARD.shiftPressed = 0;
  else KEYBOARD.shiftPressed = 1;
  renderKey();
}

var writeBs = () => {
  KEYBOARD.searchText += "\\";
  if (KEYBOARD.shiftPressed == 1) shiftKey();
  renderInput();
}

var movePointer = (x) => {
  var selEl = document.getElementById('keyboard-input');
  selEl.focus();
  if(x == "right" && selEl.selectionStart < selEl.value.length){
    selEl.setSelectionRange(selEl.selectionStart+1,selEl.selectionStart+1);
  } else if(x == "left" && selEl.selectionStart > 0){
    selEl.setSelectionRange(selEl.selectionStart-1,selEl.selectionStart-1);
  } else if(x == "up"){
    selEl.setSelectionRange(0,0);
  } else if(x == "down"){
    selEl.setSelectionRange(selEl.value.length,selEl.value.length);
  }
}
